package com.guitarz.bmi

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast

class MainActivity : AppCompatActivity(), View.OnClickListener {
    val LOG = "MAIN_ACTIVITY"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val edtHeight = findViewById<EditText>(R.id.edtHeight)
        val edtWeight = findViewById<EditText>(R.id.edtWeight)
        val btnCalculate = findViewById<Button>(R.id.btnCalculate)
        val txtResult = findViewById<TextView>(R.id.txtResult)
        //  btnHello.setOnClickListener(this)
        /* btnHello.setOnClickListener(object: View.OnClickListener {
            override fun onClick(p0: View?) {
                Toast.makeText(this@MainActivity, "Click Me", Toast.LENGTH_LONG).show()
            }

        }) */
        btnCalculate.setOnClickListener {
            Toast.makeText(this@MainActivity, "\n" + "Calculated", Toast.LENGTH_LONG).show()
            // Log.d(LOG, "Click Me")
            var height: Double = (edtHeight.text.toString().toDouble()/100)
            var weigh: Double = edtWeight.text.toString().toDouble()
            val result = weigh/(height*height)

            txtResult.text = "Result " + String.format("%.2f", result)
        }
    }

    override fun onClick(view: View?) {
        Toast.makeText(this, "Click Me", Toast.LENGTH_LONG).show()
    }
}